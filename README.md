# Mockups

This project is used for creating example screens and mockups for the Android app, Oursci website, etc.

Add **your own mockup** files inside **features** folder (and copy any resources from **templates** folder if needed), e.g:<br/>
./features/android-auto-upload.svg<br />
... and mock away! :)

Feel free to add, change and extend any design files inside **templates** folder.
